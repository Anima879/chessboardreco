import cv2
import os

WHITE_SQUARE = ((260, 131), (322, 238))
BLACK_SQUARE = ((322, 131), (378, 238))


def capture_frame(vc: cv2.VideoCapture):
    ret, frame = vc.read()
    return frame


def draw_rectangle(image):
    image = cv2.rectangle(image, BLACK_SQUARE[0], BLACK_SQUARE[1], (255, 0, 0))
    image = cv2.rectangle(image, WHITE_SQUARE[0], WHITE_SQUARE[1], (0, 255, 0))
    return image


def crop(frame):
    x = BLACK_SQUARE[0][0]
    y = BLACK_SQUARE[0][1]
    h = BLACK_SQUARE[1][1] - y
    w = BLACK_SQUARE[1][0] - x

    crop_img_b = frame[y:y + h, x:x + w]

    x = WHITE_SQUARE[0][0]
    y = WHITE_SQUARE[0][1]
    h = WHITE_SQUARE[1][1] - y
    w = WHITE_SQUARE[1][0] - x

    crop_img_w = frame[y:y + h, x:x + w]

    return crop_img_b, crop_img_w


def save_image(image, label, i, mode='train'):
    if not os.path.exists("dataset"):
        os.makedirs("dataset")

    path = f"dataset/{mode}"
    if not os.path.exists(path):
        os.makedirs(path)

    path += f"/{label}"
    if not os.path.exists(path):
        os.makedirs(path)

    image_path = f"{path}/{label}_{i}.png"
    cv2.imwrite(image_path, image)
    print(f"{image_path} saved")


def main():
    vc = cv2.VideoCapture(2)

    capture = False
    i = 0
    i_ = 0
    limit = 10
    mode = 'train'
    while True:
        frame = capture_frame(vc)

        b_square, w_square = crop(frame)

        frame = draw_rectangle(frame)
        cv2.imshow('frame', frame)

        if capture:
            if i_ == limit:
                capture = False
                print(f"Capture limit ({limit}) reached")
                i_ = 0
                continue

            if i_ > limit * 0.6:
                mode = 'valid'
            if i_ > limit * 0.9:
                mode = 'test'

            save_image(b_square, "bp", i, mode)
            save_image(w_square, "wp", i, mode)
            i += 1
            i_ += 1

        if cv2.waitKey(1) & 0xFF == ord('s'):
            if capture:
                capture = False
                print("Stop capture")
            else:
                capture = True
                print("Start capture")

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


if __name__ == '__main__':
    main()
